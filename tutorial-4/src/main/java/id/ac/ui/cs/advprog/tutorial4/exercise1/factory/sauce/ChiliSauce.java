package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class ChiliSauce implements Sauce {
    public String toString() {
        return "Chili sauce with ripped chili";
    }
}
