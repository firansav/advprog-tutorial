package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {

    // TODO Implement me!
    // What's missing in this Singleton declaration?
    static Singleton obj;

    private Singleton() {
        obj = null; 
    }

    public static Singleton getInstance() {
        // TODO Implement me!
        if (obj == null) {
            obj = new Singleton();
        }
        return obj;
    }
}