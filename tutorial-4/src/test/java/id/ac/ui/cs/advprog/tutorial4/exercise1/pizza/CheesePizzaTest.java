package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CheesePizzaTest {
    private CheesePizza depokCheesePizza;
    private CheesePizza nyCheesePizza;

    @Before
    public void setUp() {
        depokCheesePizza = new CheesePizza(new DepokPizzaIngredientFactory());
        nyCheesePizza = new CheesePizza(new NewYorkPizzaIngredientFactory());
    }

    @Test
    public void prepareDepokCheesePizzaTest()
    {
        depokCheesePizza.prepare();
        assertEquals(depokCheesePizza.dough.toString(), "ThickCrust style extra thick crust dough");

        assertEquals(depokCheesePizza.cheese.toString(), "Shredded Mozzarella");

        assertEquals(depokCheesePizza.sauce.toString(), "Marinara Sauce");
    }

    @Test
    public void prepareNewYorkCheesePizzaTest()
    {
        nyCheesePizza.prepare();
        assertEquals(nyCheesePizza.dough.toString(), "Thin Crust Dough");

        assertEquals(nyCheesePizza.cheese.toString(), "Reggiano Cheese");

        assertEquals(nyCheesePizza.sauce.toString(), "Marinara Sauce");
    }
}
