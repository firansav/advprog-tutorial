package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FrozenClams;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FrozenClamsTest {
    private FrozenClams frozenClams;

    @Before
    public void setUp() {
        frozenClams = new FrozenClams();
    }

    @Test
    public void testToString()
    {
        String expected = "Frozen Clams from Chesapeake Bay";
        assertEquals(expected, frozenClams.toString());
    }
}
