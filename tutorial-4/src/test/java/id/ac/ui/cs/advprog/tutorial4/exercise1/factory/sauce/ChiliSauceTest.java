package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.ChiliSauce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ChiliSauceTest {
    private ChiliSauce chiliSauce;

    @Before
    public void setUp() {
        chiliSauce = new ChiliSauce();
    }

    @Test
    public void testToString()
    {
        String expected = "Chili sauce with ripped chili";
        assertEquals(expected, chiliSauce.toString());
    }
}
