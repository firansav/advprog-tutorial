package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RedPepperTest {
    private RedPepper redPepper;

    @Before
    public void setUp() {
        redPepper = new RedPepper();
    }

    @Test
    public void testToString()
    {
        String expected = "Red Pepper";
        assertEquals(expected, redPepper.toString());
    }
}
