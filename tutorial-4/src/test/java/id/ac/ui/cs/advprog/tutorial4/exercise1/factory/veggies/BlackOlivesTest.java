package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BlackOlivesTest {
    private BlackOlives blackOlives;

    @Before
    public void setUp() {
        blackOlives = new BlackOlives();
    }

    @Test
    public void testToString()
    {
        String expected = "Black Olives";
        assertEquals(expected, blackOlives.toString());
    }
}
