package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DepokPizzaStoreTest {
    private DepokPizzaStore depokPizzaStore;

    @Before
    public void setUp() {
        depokPizzaStore = new DepokPizzaStore();
    }

    @Test
    public void createCheesePizzaTest()
    {
        Pizza pizza = depokPizzaStore.createPizza("cheese");
        assertEquals(pizza.getName(), "Depok Style Cheese Pizza");
    }

    @Test
    public void createClamPizzaTest()
    {
        Pizza pizza = depokPizzaStore.createPizza("clam");
        assertEquals(pizza.getName(), "Depok Style Clam Pizza");
    }

    @Test
    public void createVeggiePizzaTest()
    {
        Pizza pizza = depokPizzaStore.createPizza("veggie");
        assertEquals(pizza.getName(), "Depok Style Veggie Pizza");
    }

}
