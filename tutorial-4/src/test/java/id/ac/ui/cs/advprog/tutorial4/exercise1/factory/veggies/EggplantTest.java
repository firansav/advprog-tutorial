package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EggplantTest {
    private Eggplant eggplant;

    @Before
    public void setUp() {
        eggplant = new Eggplant();
    }

    @Test
    public void testToString()
    {
        String expected = "Eggplant";
        assertEquals(expected, eggplant.toString());
    }
}
