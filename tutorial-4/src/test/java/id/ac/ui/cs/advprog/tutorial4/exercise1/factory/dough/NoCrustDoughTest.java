package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.NoCrustDough;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NoCrustDoughTest {
    private NoCrustDough noCrustDough;

    @Before
    public void setUp() {
        noCrustDough = new NoCrustDough();
    }

    @Test
    public void testToString()
    {
        String expected = "No Crust Dough";
        assertEquals(expected, noCrustDough.toString());
    }
}
