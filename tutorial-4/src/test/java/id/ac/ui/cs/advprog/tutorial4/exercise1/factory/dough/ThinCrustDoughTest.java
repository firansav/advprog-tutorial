package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ThinCrustDoughTest {
    private ThinCrustDough thinCrustDough;

    @Before
    public void setUp() {
        thinCrustDough = new ThinCrustDough();
    }

    @Test
    public void testToString()
    {
        String expected = "Thin Crust Dough";
        assertEquals(expected, thinCrustDough.toString());
    }
}
