package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Tomato;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TomatoTest {
    private Tomato tomato;

    @Before
    public void setUp() {
        tomato = new Tomato();
    }

    @Test
    public void testToString()
    {
        String expected = "Tomato";
        assertEquals(expected, tomato.toString());
    }
}
