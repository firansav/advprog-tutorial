package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PlumTomatoSauce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PlumTomatoSauceTest {
    private PlumTomatoSauce plumTomatoSauce;

    @Before
    public void setUp() {
        plumTomatoSauce = new PlumTomatoSauce();
    }

    @Test
    public void testToString()
    {
        String expected = "Tomato sauce with plum tomatoes";
        assertEquals(expected, plumTomatoSauce.toString());
    }
}
