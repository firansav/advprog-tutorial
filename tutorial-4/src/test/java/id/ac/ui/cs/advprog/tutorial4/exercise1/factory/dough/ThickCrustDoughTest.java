package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThickCrustDough;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ThickCrustDoughTest {
    private ThickCrustDough thickCrustDough;

    @Before
    public void setUp() {
        thickCrustDough = new ThickCrustDough();
    }

    @Test
    public void testToString()
    {
        String expected = "ThickCrust style extra thick crust dough";
        assertEquals(expected, thickCrustDough.toString());
    }
}
