package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ParmesanCheese;
import org.junit.Before;
import org.junit.Test;

public class ParmesanCheeseTest {
    private ParmesanCheese parmesanCheese;

    @Before
    public void setUp() {
        parmesanCheese = new ParmesanCheese();
    }

    @Test
    public void testToString()
    {
        String expected = "Shredded Parmesan";
        assertEquals(expected, parmesanCheese.toString());
    }

}