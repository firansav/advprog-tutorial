package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class DepokPizzaIngredientFactoryTest {
    DepokPizzaIngredientFactory depokPizzaIngredientFactory;

    @Before
    public void setUp(){
        depokPizzaIngredientFactory = new DepokPizzaIngredientFactory();
    }

    @Test
    public void createDoughTest(){
        assertEquals(depokPizzaIngredientFactory.createDough().toString(), "ThickCrust style extra thick crust dough");
    }

    @Test
    public void createClamTest(){
        assertEquals(depokPizzaIngredientFactory.createClam().toString(), "Fresh Clams from Long Island Sound");
    }

    @Test
    public void createSauceTest(){
        assertEquals(depokPizzaIngredientFactory.createSauce().toString(), "Marinara Sauce");
    }

    @Test
    public void createCheeseTest(){
        assertEquals(depokPizzaIngredientFactory.createCheese().toString(), "Shredded Mozzarella");
    }

}