package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class OnionTest {
    private Onion onion;

    @Before
    public void setUp() {
        onion = new Onion();
    }

    @Test
    public void testToString()
    {
        String expected = "Onion";
        assertEquals(expected, onion.toString());
    }
}
