package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MushroomTest {
    private Mushroom mushroom;

    @Before
    public void setUp() {
        mushroom = new Mushroom();
    }

    @Test
    public void testToString()
    {
        String expected = "Mushrooms";
        assertEquals(expected, mushroom.toString());
    }
}
