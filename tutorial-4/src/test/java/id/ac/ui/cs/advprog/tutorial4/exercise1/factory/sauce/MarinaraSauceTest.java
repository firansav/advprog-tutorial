package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MarinaraSauceTest {
    private MarinaraSauce marinaraSauce;

    @Before
    public void setUp() {
        marinaraSauce = new MarinaraSauce();
    }

    @Test
    public void testToString()
    {
        String expected = "Marinara Sauce";
        assertEquals(expected, marinaraSauce.toString());
    }
}
