package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SpinachTest {
    private Spinach spinach;

    @Before
    public void setUp() {
        spinach = new Spinach();
    }

    @Test
    public void testToString()
    {
        String expected = "Spinach";
        assertEquals(expected, spinach.toString());
    }
}
