package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FreshClamsTest {
    private FreshClams freshClams;

    @Before
    public void setUp() {
        freshClams = new FreshClams();
    }

    @Test
    public void testToString()
    {
        String expected = "Fresh Clams from Long Island Sound";
        assertEquals(expected, freshClams.toString());
    }
}
