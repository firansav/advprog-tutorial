package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Garlic;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GarlicTest {
    private Garlic garlic;

    @Before
    public void setUp() {
        garlic = new Garlic();
    }

    @Test
    public void testToString()
    {
        String expected = "Garlic";
        assertEquals(expected, garlic.toString());
    }
}
