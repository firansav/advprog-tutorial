package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ClamPizzaTest {
    private ClamPizza depokClamPizza;
    private ClamPizza nyClamPizza;
    @Before
    public void setUp() {
        depokClamPizza = new ClamPizza(new DepokPizzaIngredientFactory());
        nyClamPizza = new ClamPizza(new NewYorkPizzaIngredientFactory());
    }

    @Test
    public void prepareDepokClamPizzaTest()
    {
        depokClamPizza.prepare();
        assertEquals(depokClamPizza.dough.toString(), "ThickCrust style extra thick crust dough");

        assertEquals(depokClamPizza.cheese.toString(), "Shredded Mozzarella");

        assertEquals(depokClamPizza.sauce.toString(), "Marinara Sauce");

        assertEquals(depokClamPizza.clam.toString(), "Fresh Clams from Long Island Sound");
    }

    @Test
    public void prepareNewYorkClamPizzaTest()
    {
        nyClamPizza.prepare();
        assertEquals(nyClamPizza.dough.toString(), "Thin Crust Dough");

        assertEquals(nyClamPizza.cheese.toString(), "Reggiano Cheese");

        assertEquals(nyClamPizza.sauce.toString(), "Marinara Sauce");

        assertEquals(nyClamPizza.clam.toString(), "Fresh Clams from Long Island Sound");
    }
}
