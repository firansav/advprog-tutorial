package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.CamembertCheese;
import org.junit.Before;
import org.junit.Test;

public class CamembertCheeseTest {
    private CamembertCheese camembertCheese;

    @Before
    public void setUp() {
        camembertCheese = new CamembertCheese();
    }

    @Test
    public void testToString()
    {
        String expected = "Shredded Camembert";
        assertEquals(expected, camembertCheese.toString());
    }

}