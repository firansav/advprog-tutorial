package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NewYorkPizzaStoreTest {
    private NewYorkPizzaStore newYorkPizzaStore;

    @Before
    public void setUp() {
        newYorkPizzaStore = new NewYorkPizzaStore();
    }

    @Test
    public void createCheesePizzaTest() {
        Pizza pizza = newYorkPizzaStore.createPizza("cheese");
        assertEquals(pizza.getName(), "New York Style Cheese Pizza");
    }

    @Test
    public void createClamPizzaTest() {
        Pizza pizza = newYorkPizzaStore.createPizza("clam");
        assertEquals(pizza.getName(), "New York Style Clam Pizza");
    }

    @Test
    public void createVeggiePizzaTest() {
        Pizza pizza = newYorkPizzaStore.createPizza("veggie");
        assertEquals(pizza.getName(), "New York Style Veggie Pizza");
    }
}