package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class VeggiePizzaTest{
    private VeggiePizza depokVeggiePizza;
    private VeggiePizza nyVeggiePizza;

    @Before
    public void setUp() {
        depokVeggiePizza = new VeggiePizza(new DepokPizzaIngredientFactory());
        nyVeggiePizza = new VeggiePizza(new NewYorkPizzaIngredientFactory());
    }

    @Test
    public void prepareDepokVeggiePizzaTest() {
        depokVeggiePizza.prepare();
        assertEquals(depokVeggiePizza.dough.toString(), "ThickCrust style extra thick crust dough");

        assertEquals(depokVeggiePizza.cheese.toString(), "Shredded Mozzarella");

        assertEquals(depokVeggiePizza.sauce.toString(), "Marinara Sauce");
    }

    @Test
    public void prepareNewYorkVeggiePizzaTest() {
        nyVeggiePizza.prepare();
        assertEquals(nyVeggiePizza.dough.toString(), "Thin Crust Dough");

        assertEquals(nyVeggiePizza.cheese.toString(), "Reggiano Cheese");

        assertEquals(nyVeggiePizza.sauce.toString(), "Marinara Sauce");
    }
}