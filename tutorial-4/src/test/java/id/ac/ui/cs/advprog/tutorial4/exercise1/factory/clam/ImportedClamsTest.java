package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.ImportedClams;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ImportedClamsTest {
    private ImportedClams importedClams;

    @Before
    public void setUp() {
        importedClams = new ImportedClams();
    }

    @Test
    public void testToString()
    {
        String expected = "Clams imported from North America";
        assertEquals(expected, importedClams.toString());
    }
}
